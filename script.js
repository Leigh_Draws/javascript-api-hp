async function getUsers() {

// Récupère le fichier JSON sur le Web, le chemin entre parenthèse comme argument
    const response = await fetch("https://jsonplaceholder.typicode.com/users");

// Grâce à await le script attend que la requête fetch soit terminée pour passer à la suite (je crois????)    

    const userList = await response.json();

// .map créé un nouveau tableau d'objets avec les infos des utilisateurs
    const usersAdress = userList.map(oneUser => {
        return {
            pseudo: oneUser.username,
            nom: oneUser.name,
            rue: oneUser.address.street,
            zipcode: oneUser.address.zipcode,
            city: oneUser.address.city,
        }
    })
    console.log(usersAdress);
}

getUsers()

// Première idée pour afficher les informations du tableau en liste 

// for (const oneUser of userList) {  
//     console.log("Pseudo : " + oneUser.username)
//     console.log("Nom : " + oneUser.name)
//     console.log("Rue : " + oneUser.address.street)
//     console.log("Code postal : " + oneUser.address.zipcode)
// }